
document.addEventListener("DOMContentLoaded",function(){
   var save= document.getElementById('form');

       save.addEventListener('submit', function(e) {
      const nhansu={};
        if (localStorage) {
            nhansu.ma= document.getElementById("maNV").value;
            nhansu.ho=document.getElementById("ho").value;
            nhansu.ten=document.getElementById("ten").value;
            nhansu.ngaysinh=document.getElementById("ngaysinh").value;
            nhansu.gioitinh=document.getElementById("gioitinh").value;
            nhansu.quoctich=document.getElementById("quoctich").value;
            nhansu.dantoc=document.getElementById("dantoc").value;
            nhansu.tongiao=document.getElementById("tongiao").value;
            nhansu.socmt=document.getElementById("socmt").value;
            nhansu.ngaycap= document.getElementById("ngaycap").value;
            nhansu.noicap=document.getElementById("noicap").value;
            nhansu.anh=document.getElementById("anh").value;
            nhansu.didong=document.getElementById("didong").value;
            nhansu.email=document.getElementById("email").value;
            nhansu.skype=document.getElementById("skype").value;
            nhansu.facebook=document.getElementById("facebook").value;
            nhansu.quocgia=document.getElementById("quocgia").value;
            nhansu.tinh=document.getElementById("tinh").value;
            nhansu.quanhuyen=document.getElementById("quanhuyen").value;
            nhansu.phuongxa=document.getElementById("phuongxa").value;
            nhansu.diachi= document.getElementById("diachi").value;
            nhansu.honnhan=document.getElementById("honnhan").value;
            nhansu.tongiao=document.getElementById("tongiao").value;
            nhansu.hocvan=document.getElementById("hocvan").value;
            nhansu.sothich=document.getElementById("sothich").value;
            nhansu.ngoaingu=document.getElementById("ngoaingu").value;
            nhansu.daotao=document.getElementById("daotao").value;
            nhansu.nganh=document.getElementById("nganh").value;
            nhansu.quanhe=document.getElementById("quanhe").value;
            nhansu.hoten=document.getElementById("hoten").value;
            nhansu.diachilh= document.getElementById("diachilh").value;
            
          var error=1;
         
         
          if(nhansu.ma == ""){
            document.getElementById("errorMa").innerHTML="Vui lòng nhập mã NV";
            error=1;
          }
          else{
              document.getElementById("errorMa").innerHTML="";
              error=0;  
          }
          if(nhansu.ten == ""){
            document.getElementById("errorTen").innerHTML="Vui lòng nhập tên ";
            error=1;
          }
          else{
              document.getElementById("errorTen").innerHTML="";
              error=0;
          }
         if(nhansu.ho == ""){
            document.getElementById("errorHo").innerHTML="Vui lòng nhập họ";
            error=1;
          }
          else{
              document.getElementById("errorHo").innerHTML="";
              error=0; 
          } 
          if(nhansu.ngaysinh == ""){
            document.getElementById("errorNS").innerHTML="Vui lòng nhập ngày sinh";
            error=1; 
          }
          else{
              document.getElementById("errorNS").innerHTML="";
              error=0; 
          }
         if(nhansu.quoctich == ""){
            document.getElementById("errorQT").innerHTML="Vui lòng nhập quốc tịch";
            error=1;
         }
          else{
              document.getElementById("errorQT").innerHTML="";
              error=0;
          }
          if(nhansu.socmt == ""){
            document.getElementById("errorCMT").innerHTML="Vui lòng nhập số CMND";
            error=1;
          }
         else{
              document.getElementById("errorCMT").innerHTML="";
              error=0; 
          }
          if(nhansu.email == "" ){
            document.getElementById("errorMail").innerHTML="Vui lòng nhập email";
           error=1;
          }
         else{
               document.getElementById("errorMail").innerHTML="";
              error=0;
          }
         if(error==0){
          console.log('true');
          localStorage.setItem('nhansu',JSON.stringify(nhansu));
          var ns = localStorage.getItem(JSON.parse('nhansu'));
          console.log(ns);
          return true;
          }
          else{
             e.preventDefault();
             console.log('false');
          return false;
        }
  }

    });

 function createNode(element) {
      return document.createElement(element);
  }
  function append(parent, el) {
    return parent.appendChild(el);
  }
  function remove(list) {
    while (list.hasChildNodes()) {
      list.removeChild(list.firstChild);
    }
 }

  const select = document.getElementById('quocgia');
  const select1 = document.getElementById('tinh');
  const select2 = document.getElementById('quanhuyen');
  const select3 = document.getElementById('phuongxa');

  function getQuocGia(){
       fetch('http://ums-dev.husc.edu.vn/api/tudiendulieu/quocgia/list')
      .then((resp) => resp.json())
      .then(
          function(data) {
          let quocgia = data;
          return quocgia.map(function(quocgia) {
            let option = createNode('option');
            option.innerHTML = `${quocgia.TenQuocGia}`;
            option.value= `${quocgia.MaQuocGia}`;
            append(select, option);
          }).catch(function(error) {
          console.log(error);
        })  
        })
       const ma=select.value;
       console.log(ma);
      return ma;
  }
   function getTinh(url){
      fetch(url)
      .then((resp) => resp.json())
      .then(function(data) {
          let tinh = data;
          return tinh.map(function(tinh) {
            let option = createNode('option');
            option.innerHTML = `${tinh.TenTinhThanhPho}`;
            option.value= `${tinh.MaTinhThanhPho}`;
            append(select1, option);
          }).catch(function(error) {
          console.log(error);
        })  
      })
   const ma=select1.value;
   console.log(ma);
    return ma;
   }
  function getQuanHuyen(urlh){
      fetch(urlh)
      .then((resp) => resp.json())
      .then(function(data) {
        let huyen = data;
        return huyen.map(function(huyen) {
          let option = createNode('option');
          option.innerHTML = `${huyen.TenQuanHuyen}`;
          option.value= `${huyen.MaQuanHuyen}`;
          append(select2, option);  
        })
      })
      const ma=select2.value;
      console.log(ma);
      return ma;   
}
  function getXaPhuong(x){
      fetch(x)
      .then((resp) => resp.json())
      .then(function(data) {
      let xa = data;
        return xa.map(function(xa) {
          let option = createNode('option');
          option.innerHTML = `${xa.TenPhuongXa}`;
          option.value= `${xa.MaPhuongXa}`;
          append(select3, option);  
      })
      })
   }

  getQuocGia();
  getTinh('http://ums-dev.husc.edu.vn/api/tudiendulieu/tinhthanhpho/list/001/');
  getQuanHuyen('http://ums-dev.husc.edu.vn/api/tudiendulieu/quanhuyen/list/01/');
  getXaPhuong('http://ums-dev.husc.edu.vn/api/tudiendulieu/phuongxa/list/001/');

  select.addEventListener('change', function(){
     remove(select1);
     remove(select2);
     remove(select3);
     const maqg=getQuocGia();
     const url1='http://ums-dev.husc.edu.vn/api/tudiendulieu/tinhthanhpho/list/'+maqg+'/';
     getTinh(url1); 
     const matinh=getTinh(url1);;
     const h='http://ums-dev.husc.edu.vn/api/tudiendulieu/quanhuyen/list/'+matinh+'/';
     getQuanHuyen(h);
     const maquanhuyen=getQuanHuyen(h);
     const x ='http://ums-dev.husc.edu.vn/api/tudiendulieu/phuongxa/list/'+maquanhuyen+'/';
     console.log(x);
     getXaPhuong(x);

  })
   
   select1.addEventListener('change', function(){
    remove(select2);
    const matinh=select1.value;
    const h='http://ums-dev.husc.edu.vn/api/tudiendulieu/quanhuyen/list/'+matinh+'/';
    getQuanHuyen(h);
   })
   
   select2.addEventListener('change', function(){
      remove(select3);
      const maquanhuyen=select2.value;
      const x ='http://ums-dev.husc.edu.vn/api/tudiendulieu/phuongxa/list/'+maquanhuyen+'/';
     getXaPhuong(x);
   })


   
});


   

 




  





 

  
